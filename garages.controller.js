const express = require('express');
const router = express.Router();
const { validateGarageData } = require('./validator.middleware');
// Connexion à la base de données pour faire les requêtes SQL
const connection = require("./conf/db.js");

// Définition d'une route GET pour récupérer tous les garages de la base de données
router.get("/", (req, res) => {
    // Exécution d'une requête SQL pour sélectionner tous les garages
    connection.query("SELECT * FROM garage", (err, results) => {
      // Gestion d'erreurs éventuelles lors de l'exécution de la requête
      if (err) {
        console.log(err);
        res.status(500).send("Erreur lors de la récupération des garages");
      } else {
        // Envoi des résultats sous forme de JSON si la requête est réussie
        res.json(results);
      }
    });
  });
  
  // Définition d'une route POST pour ajouter une nouveau garage dans la base de données
  router.post("/", validateGarageData, (req, res) => {
    const { name, email } = req.body;
    connection.execute(
      "INSERT INTO garage (name, email) VALUES (?, ?)",
      [name, email],
      (err, results) => {
        if (err) {
          console.log(err);
          res.status(500).send("Erreur lors de l'ajout du garage");
        } else {
          // Confirmation de l'ajout du garage avec l'ID 
          res.status(201).send(`Voiture ajoutée avec l'ID ${results.insertId}`);
        }
      }
    );
  });
  
  // Définition d'une route GET pour récupérer tous les garages de la base de données
 router.get("/:id", (req, res) => {
    const id = req.params.id;
    // Exécution d'une requête SQL pour sélectionner tous les garages
    connection.execute(
      "SELECT * FROM garage WHERE garage_id = ?",
      [id],
      (err, results) => {
        // Gestion d'erreurs éventuelles lors de l'exécution de la requête
        if (err) {
          console.log(err);
          res.status(500).send("Erreur lors de la récupération des garages");
        } else {
          // Envoi des résultats sous forme de JSON si la requête est réussie
          res.json(results);
        }
      }
    );
  });
  
  // Définition d'une route GET pour récupérer les garages par leur nom dans la base de données
 router.get("/name/:name", (req, res) => {
    const name = req.params.name;
    // Exécution d'une requête SQL pour sélectionner les garages par le nom
    connection.execute(
      "SELECT * FROM garage WHERE name = ?",
      [name],
      (err, results) => {
        // Gestion d'erreurs éventuelles lors de l'exécution de la requête
        if (err) {
          console.log(err);
          res.status(500).send("Erreur lors de la récupération d'un garage");
        } else {
          // Envoi des résultats sous forme de JSON si la requête est réussie
          res.json(results);
        }
      }
    );
  });
  
  // Définition d'une route PUT pour modifier les informations de la table garage dans la base de données
  router.put("/:id", validateGarageData, (req, res) => {
    const { name, email } = req.body;
    const id = req.params.id;
    // Exécution d'une requête SQL modifier les informations de la table garage
    connection.execute(
      "UPDATE garage SET  name = ?, email = ? WHERE garage_id = ?",
      [name, email, id],
      (err, results) => {
        // Gestion d'erreurs éventuelles lors de l'exécution de la requête
        if (err) {
          console.log(err);
          res.status(500).send("Erreur lors de la mise à jour des garages");
        } else {
          // Envoi des résultats sous forme de JSON si la requête est réussie
          res.send("garage à jour");
        }
      }
    );
  });
  
  // Route DELETE pour supprimer un garage par son ID
 router.delete("/:id", (req, res) => {
    const id = req.params.id;
    // Exécution d'une requête SQL pour supprimer un garage en particulier
    connection.execute(
      "DELETE FROM garage WHERE garage_id = ?",
      [id],
      (err, results) => {
        if (err) {
          console.log(err);
          res.status(500).send("Erreur lors de la suppression d'un garage");
        } else {
          // Confirmation de la suppression d'un garage
          res.send(`Garage supprimé`);
        }
      }
    );
  });

module.exports = router;
