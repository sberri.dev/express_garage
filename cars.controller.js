const express = require('express');
const router = express.Router();
// Il est necessaire d'avoir la connexion à la base de données pour faire les requêtes SQL
const connection = require("./conf/db.js");
const { validateCarData } = require('./validator.middleware');

// Définition d'une route GET pour récupérer toutes les voitures de la base de données
router.get("/", (req, res) => {
    // Exécution d'une requête SQL pour sélectionner toutes les voitures
    connection.query("SELECT * FROM car", (err, results) => {
      // Gestion d'erreurs éventuelles lors de l'exécution de la requête
      if (err) {
        console.log(err);
        res.status(500).send("Erreur lors de la récupération des voitures");
      } else {
        // Envoi des résultats sous forme de JSON si la requête est réussie
        res.json(results);
      }
    });
  });
  
  // Définition d'une route POST pour ajouter une nouvelle voiture dans la base de données
  router.post("/", validateCarData, (req, res) => {
    const { brand, model } = req.body;
    connection.execute(
      "INSERT INTO car (brand, model) VALUES (?, ?)",
      [brand, model],
      (err, results) => {
        if (err) {
          console.log(err);
          res.status(500).send("Erreur lors de l'ajout de la voiture");
        } else {
          // Confirmation de l'ajout de la voiture avec l'ID de la voiture ajoutée
          res.status(201).send(`Voiture ajoutée avec l'ID ${results.insertId}`);
        }
      }
    );
  });
  
  // Définition d'une route GET pour récupérer toutes les voitures de la base de données
 router.get("/:id", (req, res) => {
    const id = req.params.id;
    // Exécution d'une requête SQL pour sélectionner toutes les voitures
    connection.execute(
      "SELECT * FROM car WHERE car_id = ?",
      [id],
      (err, results) => {
        // Gestion d'erreurs éventuelles lors de l'exécution de la requête
        if (err) {
          console.log(err);
          res.status(500).send("Erreur lors de la récupération des voitures");
        } else {
          // Envoi des résultats sous forme de JSON si la requête est réussie
          res.json(results);
        }
      }
    );
  });
  
  // Définition d'une route GET pour récupérer toutes les voitures de la base de données
 router.get("/brand/:brand", (req, res) => {
    const brand = req.params.brand;
    // Exécution d'une requête SQL pour sélectionner toutes les voitures
    connection.execute(
      "SELECT * FROM car WHERE brand = ?",
      [brand],
      (err, results) => {
        // Gestion d'erreurs éventuelles lors de l'exécution de la requête
        if (err) {
          console.log(err);
          res.status(500).send("Erreur lors de la récupération des voitures");
        } else {
          // Envoi des résultats sous forme de JSON si la requête est réussie
          res.json(results);
        }
      }
    );
  });
  
  // Définition d'une route PUT pour récupérer toutes les voitures de la base de données
  router.put("/:id", validateCarData, (req, res) => {
    const { brand, model } = req.body;
    const id = req.params.id;
    // Exécution d'une requête SQL pour sélectionner toutes les voitures
    connection.execute(
      "UPDATE car SET  brand = ?, model = ? WHERE car_id = ?",
      [brand, model, id],
      (err, results) => {
        // Gestion d'erreurs éventuelles lors de l'exécution de la requête
        if (err) {
          console.log(err);
          res.status(500).send("Erreur lors de la mise à jour des voitures");
        } else {
          // Envoi des résultats sous forme de JSON si la requête est réussie
          res.send("voiture à jour");
        }
      }
    );
  });
  
  // Route DELETE pour supprimer une voiture par son ID
 router.delete("/:id", (req, res) => {
    const id = req.params.id;
    // Exécution d'une requête SQL pour supprimer une voiture en particulier
    connection.execute(
      "DELETE FROM car WHERE car_id = ?",
      [id],
      (err, results) => {
        if (err) {
          console.log(err);
          res.status(500).send("Erreur lors de la suppression d une voiture");
        } else {
          // Confirmation de la suppression d'une voiture
          res.send(`Voiture supprimée.`);
        }
      }
    );
  });

module.exports = router;
