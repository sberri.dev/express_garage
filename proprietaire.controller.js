const express = require('express');
const router = express.Router();
// Connexion à la base de données pour faire les requêtes SQL
const connection = require("./conf/db.js");
const { validateProprietaireData } = require('./validator.middleware');

// Définition d'une route GET pour récupérer tous les propriètaires de la base de données
router.get("/", (req, res) => {
    // Exécution d'une requête SQL pour sélectionner tous les garages
    connection.query("SELECT * FROM proprietaire", (err, results) => {
      // Gestion d'erreurs éventuelles lors de l'exécution de la requête
      if (err) {
        console.log(err);
        res.status(500).send("Erreur lors de la récupération des propriètaires");
      } else {
        // Envoi des résultats sous forme de JSON si la requête est réussie
        res.json(results);
      }
    });
  });

  // Définition d'une route GET pour afficher les voitures d'un propriètaire
router.get("/:proprietaire_id/car", (req, res) => {
    const idProprietaire = req.params.proprietaire_id
    // Exécution d'une requête SQL pour sélectionner tous les garages
    connection.query("SELECT * FROM car WHERE proprietaire_id = ?", [idProprietaire], (err, results) => {
      // Gestion d'erreurs éventuelles lors de l'exécution de la requête
      if (err) {
        console.log(err);
        res.status(500).send("Erreur lors de la récupération des voitures d'un propriètaire");
      } else {
        // Envoi des résultats sous forme de JSON si la requête est réussie
        res.json(results);
      }
    });
  });

    // Définition d'une route POST pour un nouveau proprietaire dans la base de données
    router.post("/", validateProprietaireData, (req, res) => {
        const name = req.body;
        connection.execute(
          "INSERT INTO proprietaire (name) VALUES (?)",
          [name],
          (err, results) => {
            if (err) {
              console.log(err);
              res.status(500).send("Erreur lors de l'ajout d'un proprietaire");
            } else {
              // Confirmation de l'ajout du proprietaire avec l'ID 
              res.status(201).send(`Propriètaire ajoutée avec l'ID ${results.insertId}`);
            }
          }
        );
      });
      

  module.exports = router;
