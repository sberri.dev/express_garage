// fait appel à la bibliothèque dotenv et charge les variables d'environnement à partir du fichier .env
require("dotenv").config();
// Importation de la bibliothèque cors
const cors = require("cors");
// Importation de la librairie Express
const express = require("express")
const connection = require("./conf/db.js");
const app = express();

// Configure l'application pour parser les requêtes avec des corps au format JSON.
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Configuration de l'application pour autoriser une origine particulière
app.use(
  cors({
    origin: "http://localhost:8080",
  })
);

// Importation du middleware logger
const { logRequest } = require('./logger.middleware');

// Utilisation du middleware pour logger les requêtes
app.use(logRequest);

// Importation du contrôleur de cars, garages, proprietaires et auth
const carsController = require('./cars.controller.js');
const garagesController = require('./garages.controller.js');
const proprietairesController = require('./proprietaire.controller.js');
const authController = require('./auth.controller.js');

// Utilisation du contrôleur pour gérer les routes "cars", "garage", "proprietaire" et "auth"
app.use('/cars', carsController);
app.use('/garages', garagesController);
app.use('/proprietaires', proprietairesController);
app.use('/auth', authController);

//definition du port 3000 sur lequel le serveur écoutera les requetes HTTP
const port = 3000;


// Démarrarage de serveur Express et écoute sur le port spécifié
app.listen(port, () => {
  console.log(`Serveur en écoute sur le port ${port}`);
});


