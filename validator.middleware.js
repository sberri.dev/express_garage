//middleware de validation
const validateCarData = (req, res, next) => {
  const { brand, model } = req.body;

  // Vérification de la présence du titre et auteur dans la requête
  if (!brand || !model) {
    return res
      .status(400)
      .json({ message: "La marque et le modèle sont obligatoires." });
  }

  next();
};

const validateGarageData = (req, res, next) => {
  const { name, email } = req.body;

  // Vérification de la présence du nom et email dans la requête
  if (!name || !email) {
    return res
      .status(400)
      .json({ message: "Le nom et l'email sont obligatoires." });
  }

  // La fonction qui permet de verifier le format de mail avec regex
  const validateEmail = (email) => {
    const emailRegex = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    return emailRegex.test(email);
  };

  // Pour vérifier le format de l'email
  if (!validateEmail(email)) {
    return res
      .status(400)
      .json({ message: "Le format de l'email est incorrect." });
  }

  next();
};

//middleware de validation
const validateProprietaireData = (req, res, next) => {
  const { name } = req.body;

  // Vérifier que le nom est présent dans la requête
  if (!name) {
    return res
      .status(400)
      .json({ message: "Le nom est obligatoire." });
  }
  
  next();
};

module.exports = { validateCarData, validateGarageData, validateProprietaireData };
